require 'socket'
require 'forgery'
require 'timeout'

#perhaps change this later to a threaded version.

a = TCPSocket.new('127.0.0.1', 12322)
loop do 

  #check for messages
  begin
	Timeout::timeout(0.1){
	  	puts a.recv(100000)
	}
  rescue Timeout::Error
  end

  #Shout Out any Messages
  begin
  Timeout::timeout(0.1){
	  a.puts gets.chomp
  }
  rescue Timeout::Error
  end

  sleep (0.4)	#let timeouts expire
end
a.close
