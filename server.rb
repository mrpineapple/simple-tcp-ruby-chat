require 'socket'
require 'forgery'	#requires the Forgery Gem

connections = Array.new  
 
Socket.tcp_server_loop(12322) do |conn, addr|
  Thread.new do
    client = "#{addr.ip_address}:#{addr.ip_port}"	#not used, but handy sure.

    client_name = Forgery(:name).female_first_name	#generate a random Female name for this Client :) 
    puts "#{client_name} is connected"
    connections.push(conn)							#add our connection to the list so we can broadcast to them later

    begin
      loop do
        line = conn.readline					
        puts "#{client_name} says: #{line}"		

        i = connections.length()
        j = 0

        until i == j 								#for all connections
	      connections[j].puts("#{client_name}: #{line}")	
          j=j+1
        end

      end
    rescue EOFError
      conn.close
#	  connections.pop(conn)
      puts "#{client_name} has disconnected"
    end
  end
end
